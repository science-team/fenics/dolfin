# MPI tests are set up to run on 3 processes.
N_MPI=3
export PRTE_MCA_plm_ssh_agent=/bin/false
export PRTE_MCA_rmaps_default_mapping_policy=:oversubscribe
export OMPI_MCA_btl_base_warn_component_unused=0

export PETSC_DIR=/usr/lib/petsc64
export SLEPC_DIR=/usr/lib/slepc64

DEB_HOST_ARCH=$( dpkg-architecture -q DEB_HOST_ARCH )

PYVER=$(py3versions -sv)
case " armel riscv64 " in \
  *\ ${DEB_HOST_ARCH}\ *) PYVER=$(py3versions -dv);; \
esac

# the -k keyword option doesn't handle the hyphen in navier-stokes or cahn-hilliard with multiple tests skipped
# so need to identify by single name
TESTS_SKIPPED=cahn
MULTIPLE_TESTS_SKIPPED="hyperelasticity elastodynamics navier elasticity"

case " i386 armhf " in \
  *\ ${DEB_HOST_ARCH}\ *) EXTRA_TESTS_SKIPPED="iterative block meshview assignment";; \
esac

case " s390x " in \
  *\ ${DEB_HOST_ARCH}\ *) EXTRA_TESTS_SKIPPED="matnest";; \
esac

for test in $MULTIPLE_TESTS_SKIPPED $EXTRA_TESTS_SKIPPED; do
  TESTS_SKIPPED="$TESTS_SKIPPED or $test"
done
TEST_KEYWORDS="not ( $TESTS_SKIPPED )"

python3 python/demo/generate-demo-files.py

for pyver in $PYVER; do
    echo "=== python $pyver 64-bit demo test (serial) ==="
    DOLFIN_LIB_NAME_EXT=64 python$pyver -m pytest -v -k "${TEST_KEYWORDS}" python/demo/test.py
done

for pyver in $PYVER; do
    echo "=== python $pyver 64-bit demo test (MPI) ==="
    DOLFIN_LIB_NAME_EXT=64 python$pyver -m pytest -v -k "${TEST_KEYWORDS}" python/demo/test.py --mpiexec=mpiexec --num-proc=${N_MPI}
done
