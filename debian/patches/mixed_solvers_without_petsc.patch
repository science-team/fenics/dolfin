--- a/dolfin/fem/MixedLinearVariationalSolver.cpp
+++ b/dolfin/fem/MixedLinearVariationalSolver.cpp
@@ -228,6 +228,7 @@
 {
   begin("Solving mixed linear variational problem.");
 
+#ifdef HAS_PETSC
   // Get parameters
   std::string solver_type   = parameters["linear_solver"];
   const std::string pc_type = parameters["preconditioner"];
@@ -301,6 +302,9 @@
   // Ghost values have to be updated in parallel since they are not updated by solve
   for(auto &u : us)
     as_type<PETScVector>(u)->update_ghost_values();
+#else
+  dolfin_error("MixedLinearVariationalSolver.cpp", "solve problem", "MixedLinearVariationalSolver::solve requires PETSc");
+#endif
 
 #if 0 // Configure PCFieldSplit (Not working yet)
   PETScOptions::set("pc_type", "fieldsplit");
--- a/dolfin/fem/MixedNonlinearVariationalSolver.cpp
+++ b/dolfin/fem/MixedNonlinearVariationalSolver.cpp
@@ -61,6 +61,7 @@
 {
   begin("Solving mixed nonlinear variational problem.");
 
+#ifdef HAS_PETSC
   // Check that the Jacobian has been defined
   dolfin_assert(_problem);
   if (!_problem->has_jacobian())
@@ -70,14 +71,12 @@
                  "The Jacobian form has not been defined");
   }
   // Check that the dolfin is configured with petsc is bounds are set
-#ifndef HAS_PETSC
   if (_problem->has_lower_bound() || _problem->has_upper_bound())
   {
     dolfin_error("NonlinearVariationalSolver.cpp",
                  "solve nonlinear variational problem",
                  "Needs PETSc to solve bound constrained problems");
   }
-#endif
 
   // Get problem data
   dolfin_assert(_problem);
@@ -156,7 +155,6 @@
     dolfin_assert(nonlinear_problem);
     ret = newton_solver->solve(*nonlinear_problem, *x);
   }
-#ifdef HAS_PETSC
   // FIXME : SNES Solver need to be setup (SNES norm is zero at 1st iteration) 
   else if (std::string(parameters["nonlinear_solver"]) == "snes")
   {
@@ -185,16 +183,20 @@
       ret = snes_solver->solve(*nonlinear_problem, *x);
     }
   }
-#endif
   else
   {
     dolfin_error("MixedNonlinearVariationalSolver.cpp",
                  "solve nonlinear variational problem",
                  "Unknown nonlinear solver type");
   }
+#else
+  dolfin_error("MixedNonlinearVariationalSolver.cpp", "solve problem", "MixedNonlinearVariationalSolver::solve requires PETSc");
+#endif
 
   end();
+#ifdef HAS_PETSC
   return ret;
+#endif
 }
 //-----------------------------------------------------------------------------
 // Implementation of NonlinearDiscreteProblem
@@ -215,6 +217,7 @@
 void MixedNonlinearVariationalSolver::
 MixedNonlinearDiscreteProblem::F(GenericVector& b, const GenericVector& x)
 {
+#ifdef HAS_PETSC
   const auto bform = _problem->residual_form();
   auto u = _problem->solution();
   auto bcs = _problem->bcs();
@@ -262,11 +265,15 @@
 
   // Update b from _bs[]
   J.init_vectors(b, _bs);
+#else
+  dolfin_error("MixedNonlinearVariationalSolver.cpp", "assemble residual", "MixedNonlinearVariationalSolver::F requires PETSc");
+#endif
 }
 //-----------------------------------------------------------------------------
 void MixedNonlinearVariationalSolver::
 MixedNonlinearDiscreteProblem::J(GenericMatrix& A, const GenericVector& x)
 {
+#ifdef HAS_PETSC
   const auto jform = _problem->jacobian_form();
   auto u = _problem->solution();
   auto bcs = _problem->bcs();
@@ -321,4 +328,7 @@
       petsc_mats[i*u.size() + j] = as_type<PETScMatrix>(_Js[i*u.size() + j])->mat();
   as_type<PETScMatrix>(A).set_nest(petsc_mats);
   as_type<PETScMatrix>(A).apply("insert");
+#else
+  dolfin_error("MixedNonlinearVariationalSolver.cpp", "assemble jacobian", "MixedNonlinearVariationalSolver::J requires PETSc");
+#endif
 }
--- a/dolfin/nls/NewtonSolver.cpp
+++ b/dolfin/nls/NewtonSolver.cpp
@@ -156,11 +156,13 @@
   }
 
   // Might need a copy of _matA (nest -> aij)
+#ifdef HAS_PETSC
   std::shared_ptr<GenericMatrix> _matAc;
 
   bool need_mataij_conversion = (solver_type == "lu" or solver_type == "direct" or solver_type == "default" or solver_type == "mumps");
   if (as_type<PETScMatrix>(_matA)->is_nest() and need_mataij_conversion)
     log(WARNING, "Converting PETScNestMatrix into AIJ (due to direct solver)");
+#endif
 
   // Start iterations
   while (!newton_converged && _newton_iteration < maxiter)
@@ -170,6 +172,7 @@
     nonlinear_problem.J_pc(*_matP, x);
 
     // Setup (linear) solver (including set operators)
+#ifdef HAS_PETSC
     if (as_type<PETScMatrix>(_matA)->is_nest() and need_mataij_conversion)
     {
       log(TRACE, "NewtonSolver: setup matrix with MATAIJ type in direct solver");
@@ -179,6 +182,9 @@
     }
     else
       solver_setup(_matA, _matP, nonlinear_problem, _newton_iteration);
+#else
+    solver_setup(_matA, _matP, nonlinear_problem, _newton_iteration);
+#endif
 
     // Perform linear solve and update total number of Krylov
     // iterations
